package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Classe de Test unitaire faite maison
 */
public class StreamingTest {

    private static final Logger LOG = LogManager.getLogger(StreamingTest.class);

    /**
     * Test unitaire standard
     * Enfin basiquement oui, mais il serait bon d'exploiter d'avantage cet outil
     * => pour la suite,
     *   * utiliser le système d'Assertions Junit plutôt que jdk
     *   * faire n Méthodes de tests plutôt qu'une seule...
     *   * gérer éventuellement les initialisations d'objets pre-tests et post-tests
     */
     @Test
     public void allTests() {
        try {

            File image = new File("petite_image_2.png");
            ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());

            // Sérialisation
            ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
            serializer.serialize(image, media);

            String encodedImage = media.getEncodedImageOutput().toString();
            LOG.info(encodedImage + "\n");

            // Désérialisation
            ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
            ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);

            deserializer.deserialize(media);
            byte[] deserializedImage = ((ByteArrayOutputStream)deserializer.getSourceOutputStream()).toByteArray();
            // Vérification
            // 1/ Automatique
            byte[] originImage = Files.readAllBytes(image.toPath());
            assert Arrays.equals(originImage, deserializedImage);
            LOG.info("Cette sérialisation est bien réversible :)");

            //  2/ Manuelle
            File extractedImage = new File("petite_image_extraite.png");
            new FileOutputStream(extractedImage).write(deserializedImage);
            LOG.info("Je peux vérifier moi-même en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le répertoire de ce Test");

        } catch (IOException e) {
            LOG.error(e);
        }
    }

}
