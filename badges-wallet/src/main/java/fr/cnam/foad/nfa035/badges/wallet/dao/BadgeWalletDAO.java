package fr.cnam.foad.nfa035.badges.wallet.dao;


import java.io.*;


/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet à badge unique
 */
public class BadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(BadgeWalletDAO.class);

    private File walletDatabase;
    private ImageFileFrame media;

    /**
     * Constructeur élémentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public BadgeWalletDAO(String dbPath) throws IOException{
        this.walletDatabase = new File(dbPath);
        this.media = new ImageFileFrame(walletDatabase);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException{
        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException{
        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(imageStream);
        deserializer.deserialize(media);
    }

}
